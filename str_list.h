// Time-stamp: <2021-03-18 11:14:01 daniel>
#ifndef __STR_LIST_H__
#define __STR_LIST_H__


//
// str_list.h
//


// Required header files
//-----------------------------------------------------------------------------
#include <stdbool.h>  // defined bool, true and false


// If error_t is not yet defined - prevent including another header
#ifndef __error_t_defined
#define __error_t_defined true
typedef int error_t;
#endif

// Define a string str_list to store command line arguments
//-----------------------------------------------------------------------------
typedef struct {

  char* string;  // single memory array holds all strings
  size_t length;  // length of the memory holding all strings
  error_t error;  // holds value of the last

} str_list_t;


// Functions used to manipulate string str_lists - see 'str_list.c' for details
//-----------------------------------------------------------------------------
void str_list_init( str_list_t* );
bool str_list_isempty( str_list_t* );
error_t str_list_add( str_list_t*, const char* );
error_t str_list_add_sep( str_list_t*, const char*, int );
error_t str_list_append( str_list_t*, str_list_t* );
char* str_list_next( str_list_t*, const char* );
void str_list_free( str_list_t* );
size_t str_list_count( str_list_t* );
char* str_list_stringify( str_list_t*, int );
void str_list_foreach( str_list_t*, void (*)( const char* ) );
void str_list_foreach_param( str_list_t*, void(*)( const char*, void* ), void* );

#endif  // __STR_LIST_H__
